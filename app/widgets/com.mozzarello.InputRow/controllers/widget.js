//Widget for an Input Row
//Provides a label, textField, statusIndicator and errorLabel
var _args = arguments[0] || {};

var configureWidget = function(){
	for (var key in _args){
		if(_args.hasOwnProperty(key)){
			if (key == "titleText"){
				$.titleLabel.text = _args[key];
				$.titleLabel.accessibilityLabel = _args[key];
			} else if (key == "hintText"){
				$.textField.hintText = _args[key];
				$.textField.accessibilityLabel = _args[key];
			} else if (key == "autocorrect"){
				$.textField.autocorrect = _args[key];
			} else if (key == "autocapitalization"){
				//BUG: There seems to be a bug with autocapitalization, it doesn't work in iOS 8.4 & Android 
				//It also translates my strings to integers. I haven't seen any documentation about this online 
				if(_args[key] == 0){ //none
					$.textField.autocapitalization =  0;
				} else if(_args[key] == 1){ //words
					$.textField.autocapitalization =  Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS;
				} else if(_args[key] == 3){ //all
					$.textField.autocapitalization =  Titanium.UI.TEXT_AUTOCAPITALIZATION_ALL;
				} else if(_args[key] == 2){ //sentences
					$.textField.autocapitalization =  Titanium.UI.TEXT_AUTOCAPITALIZATION_SENTENCES;
				} 
			} else if (key == "passwordMask"){
				$.textField.passwordMask = _args[key];
			} else if (key == "accessibilityHint"){
				$.titleLabel.accessibilityHint = _args[key];
			} else if (key == "required"){
				$.statusLabel.text = "Required";
			}			
		}
	}
};

exports.init = function(){
	configureWidget();
};

exports.checkForError = function (isError,errorMessage){
	if(isError){
		$.statusIndicator.backgroundColor = "#E25151";
		$.statusLabel.color = "#E25151";
		$.statusLabel.accessibilityLabel = errorMessage;
	} else {
		$.statusIndicator.backgroundColor = "#90C290";
		$.statusLabel.color = "#888";
		$.statusLabel.accessibilityLabel = null;
	}
	
	$.statusLabel.text = errorMessage;
};
