var firstNameLengthLimit = 25;
var lastNameLengthLimit = 25;
var usernameLengthLimit = 16;
var passwordLengthLimit = 16;
var submitEnabled = false;

var checkForSubmitEnable = function(){
	if($.firstName.isValidlyFilled && 
		$.lastName.isValidlyFilled && 
		$.username.isValidlyFilled && 
		$.password.isValidlyFilled && 
		$.passwordConfirmation.isValidlyFilled) {
			$.submitButton.submissionEnabled = true;
			$.submitButton.color = "#fff";	
		} else {
			$.submitButton.submissionEnabled = false;
			$.submitButton.color = "#DDD";	
		}
};

//Password confirmation must be checked both when you are typing and when the password field is typed
var checkPasswordConfirmation = function(){
	var isError = false;
	var errorMessage;
	
	if (isError == false && $.passwordConfirmation.textField.value != $.password.textField.value){
		isError = true;
		errorMessage = "Password doesn't match";
	}
	
	if(isError == false && $.passwordConfirmation.textField.length == 0){
		isError = true;
		errorMessage = "Required";
	}
	
	$.passwordConfirmation.isValidlyFilled = !isError;
	$.passwordConfirmation.checkForError(isError,errorMessage);
	checkForSubmitEnable();
};


//UI Components functions and configuration
$.firstName.init();
$.firstName.isValidlyFilled = false;
$.firstName.textField.addEventListener("change", function(e){	
	var isError = false;
	var errorMessage;
	
	if (isError == false && e.value.length > firstNameLengthLimit){
		isError = true;
		errorMessage = "Must be less than "+firstNameLengthLimit+" characters";
	}
	
	if(isError == false && e.value.length == 0){
		isError = true;
		errorMessage = "Required";
	}
	
	if (isError == false && !(/^[a-zA-Z]+$/.test(e.value))){
		isError = true;
		errorMessage = "Only letters please";
	}
	
	$.firstName.isValidlyFilled = !isError;
	$.firstName.checkForError(isError,errorMessage);
	checkForSubmitEnable();
});

$.lastName.init();
$.lastName.isValidlyFilled = false;
$.lastName.textField.addEventListener("change", function(e){
	var isError = false;
	var errorMessage;
	
	if (isError == false && e.value.length > lastNameLengthLimit){
		isError = true;
		errorMessage = "Must be less than "+lastNameLengthLimit+" characters";
	}
	
	if(isError == false && e.value.length == 0){
		isError = true;
		errorMessage = "Required";
	}
	
	if (isError == false && !(/^[a-zA-Z]+$/.test(e.value))){
		isError = true;
		errorMessage = "Only letters please";
	}
	
	$.lastName.isValidlyFilled = !isError;
	$.lastName.checkForError(isError,errorMessage);
	checkForSubmitEnable();
});

$.username.init();
$.username.isValidlyFilled = false;
$.username.textField.addEventListener("change", function(e){	
	var isError = false;
	var errorMessage;
	
	if (isError == false && e.value.length > usernameLengthLimit){
		isError = true;
		errorMessage = "Must be less than "+ usernameLengthLimit+" characters";
	}
	
	if(isError == false && e.value.length == 0){
		isError = true;
		errorMessage = "Required";
	}
	
	$.username.isValidlyFilled = !isError;
	$.username.checkForError(isError,errorMessage);
	checkForSubmitEnable();
});

$.password.init();
$.password.isValidlyFilled = false;

//On Android only this event gets called when the app boots making the statusBar go red
//Using this variable we watch and prevent that. Not an ideal solution but ok for now.
$.password.isInitialized = false;
$.password.textField.addEventListener("change", function(e){	
	if($.password.isInitialized == true){
		var isError = false;
		var errorMessage;
		
		if (isError == false && e.value.length > passwordLengthLimit){
			isError = true;
			errorMessage = "Must be less than "+passwordLengthLimit+" characters";
		}
		
		if(isError == false && e.value.length == 0){
			isError = true;
			errorMessage = "Required";
		}
			
		$.password.isValidlyFilled = !isError;
		$.password.checkForError(isError,errorMessage);
		checkPasswordConfirmation();
		checkForSubmitEnable();
	} else if($.password.isInitialized == false && e.value.length == 0){	
		$.password.isInitialized = true;
	}
});

$.passwordConfirmation.init();
$.passwordConfirmation.isValidlyFilled = false;
$.passwordConfirmation.textField.addEventListener("change", function(e){	
	checkPasswordConfirmation();
});


$.submitButton.submissionEnabled = false;
$.submitButton.addEventListener("click", function(e){
	if($.submitButton.submissionEnabled == true){
		$.activityIndicator.show();
	    setTimeout(function(){
	    	
	    	//Simulating Random quality of connection with Server
	    	var randomNumber = Math.random();
	    	if(randomNumber > 0.5){
	    		//Successful connection
	    		alert("Success!");
	    	} else {
	    		//Failed connection connection
	    		alert("There was an error connecting. Please try again.");
	    	}
	        $.activityIndicator.hide();
	        checkForSubmitEnable();
	        $.submitButton.visible = true;
	    }, 3000);
	    $.submitButton.visible = false;
	} else {
		alert("Please complete all the fields properly");
	}
});

//The scrollview is present for two reasons: If content (mainContainer) is taller than the screen it allows to scroll
//and if the keyboard comes up the scrollview will automatically move the content to the right point to
//see what you are typing.
//When the content is smaller than the screen size the scrolling is automatically disabled. So we have to
//set the contentHeight of the ScrollView one pixel bigger than the screen to make sure it still scrolls when the keyboard
//appears after the mainContainer has been layed out. This happens for example in iPhone 5/5s
$.mainContainer.addEventListener("postlayout", function (e){
	if($.mainContainer.size.height < Alloy.Globals.PlatformHeight){
		$.mainScrollView.contentHeight = Alloy.Globals.PlatformHeight+1;
	}
});


$.index.open();